/// <references types="Cypress" />

import "../../../support/settings/category/addCategory"
import "../../../support/settings/category/editCategory"
import "../../../support/settings/category/deleteCategory"

context('Login', () => {
    
  var faker                         = require('faker')
  var categoryName                  = faker.random.word()
  var categoryDescription           = faker.lorem.sentence()
  var EditedcategoryName            = faker.random.word()
  var EditedcategoryDescription     = faker.lorem.sentence()

    it('Success create new category at dev', () => {
      cy.addCategory(categoryName,categoryDescription) 
    })

    it('Success edit category at dev', ()=>{
      cy.editCategory(categoryName, EditedcategoryName, EditedcategoryDescription)
    })

    it('Succes delete category at dev', ()=>{
      cy.deleteCategory(EditedcategoryName)
    })
  })