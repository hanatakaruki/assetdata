// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

import 'cypress-file-upload';

Cypress.config('defaultCommandTimeout', 10000)

Cypress.Commands.add('DevLogin', () => {
    cy.get('input[placeholder="Enter Email"]').type(Cypress.config('DevEmail')).should('have.value', Cypress.config('DevEmail'))
    cy.get('input[placeholder="Enter Password"]').type(Cypress.config('DevPassword'))
    cy.get('button').contains('SIGN IN').click() 
    cy.wait(100)
    cy.url().should('include', '/dashboard')
    })

Cypress.Commands.add('StagingLogin', () => {
    cy.get('input[placeholder="Type your email"]').type(Cypress.config('StagingEmail')).should('have.value', Cypress.config('StagingEmail'))
    cy.get('input[placeholder="Type your password"]').type(Cypress.config('StagingPassword'))
    cy.get('button').contains('SIGN IN').click()
    cy.wait(100)
    cy.url().should('include', '/dashboard')
    })

Cypress.Commands.add('forceVisit', url => {
    cy.get('body').then(body$ => {
      const appWindow = body$[0].ownerDocument.defaultView;
      const appIframe = appWindow.parent.document.querySelector('iframe');
  
      // We return a promise here because we don't want to
      // continue from this command until the new page is
      // loaded.
      return new Promise(resolve => {
        appIframe.onload = () => resolve();
        appWindow.location = url;
      });
    });
  });
  
  const COMMAND_DELAY = 500;
  
  for (const command of ['visit', 'click', 'trigger', 'type', 'clear', 'reload', 'contains']) {
      Cypress.Commands.overwrite(command, (originalFn, ...args) => {
          const origVal = originalFn(...args);
  
          return new Promise((resolve) => {
              setTimeout(() => {
                  resolve(origVal);
              }, COMMAND_DELAY);
          });
      });
  }

