Cypress.Commands.add('LogoutfromDev', ()=>{
    cy.get('.kt-header__topbar-wrapper').click() 
    cy.get('.kt-header__topbar-item > .dropdown-menu ').contains('Logout').click() 
    cy.url().should('include', '/login')
})