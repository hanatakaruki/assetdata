require('cypress-xpath')

Cypress.Commands.add('editCategory', (categoryName, EditedcategoryName,EditedcategoryDescription)=>{
    cy.get('div > .ml-3').click()
    cy.get('.css-1hwfws3').click().wait(10).type(categoryName, { delay: 100 })
    .wait(10).type('{enter}')
    cy.get('#react-select-3-option-23').click()
    cy.xpath('(//i[contains(@class,"styles-module__primaryIcon___3AGrC styles-module__icon___1YMjL")])[2]').click().wait(10)
    cy.get('#name').type(EditedcategoryName)
    cy.get('#description').type(EditedcategoryDescription)
    cy.get('button').contains('Save').click()
    cy.get('.Toastify__toast-body').contains('Category '+EditedcategoryName+' was updated').should('be.visible')
})