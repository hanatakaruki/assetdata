require('cypress-xpath')

Cypress.Commands.add('deleteCategory', (EditedcategoryName)=>{
    cy.get('div > .ml-3').click()
    cy.get('.css-1hwfws3').click().wait(10).type(EditedcategoryName, { delay: 100 })
    .wait(10).type('{enter}')
    cy.get('#react-select-3-option-23').click()
    cy.xpath('//i[contains(@class,"styles-module__dangerIcon___1d1eF styles-module__icon___1YMjL")]').click().wait(10)
    cy.get('button').contains('Delete').click()
    cy.get('.Toastify__toast-body').contains('Category '+EditedcategoryName+' was Deleted').should('be.visible')
})