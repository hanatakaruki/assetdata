Cypress.Commands.add('addCategory', (categoryName,categoryDescription)=>{
    cy.visit(Cypress.config('DevURL'));
    cy.DevLogin()
    cy.get('[data-target="#setup"]').click()
    cy.get('[data-target="#settings"]').click()
    cy.get('a').contains('categories').click()
    cy.wait(30)
    cy.get('.kt-portlet__head-title').contains('Categories').should('be.visible')
    cy.get('.kt-portlet__head-label > div > .styles-module__primaryBtnMetronic___3_N91').click()
    cy.get('#name').type(categoryName)
    cy.get('#description').type(categoryDescription)
    cy.get('button').contains('Save').click()
    cy.get('.Toastify__toast-body').contains('Category '+categoryName+' was created').should('be.visible')
})